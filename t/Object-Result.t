=pod

=encoding utf8

=head1 SUMMARY

Unit Test for Object::Result
since 2017/08/23 by H.Seo

=head1 USAGE

```
	$ cd <above-your-lib>
	$ perl -Ilib <path-to-this-script>
```

=cut

use strict;
use warnings;

use Test::More;
use Test::Exception;
BEGIN
{
	eval 'use Test::More::Color "foreground"';
}

use Scalar::Util qw/blessed looks_like_number/;
# use Data::Compare;	# import Compare

use Data::Dumper;
$Data::Dumper::Terse	= 1;
$Data::Dumper::Sortkeys	= 1;
$Data::Dumper::Indent	= 2;

{
	BEGIN
	{
		BAIL_OUT("use Object::Result faild.") unless use_ok('Object::Result');
	}
	
	# 基本的な success オブジェクトテスト
	my $success = Object::Result->create_success();

	ok( $success ,'create_success');
	ok( blessed $success ,'blessed create_success');
	ok( ! $success->error_code  ,'! create_success->error_code');
	ok( ! $success->error_message  ,'! create_success->error_message');
	ok( length "$success" , "length \"$success\" (stringify test)");

	# 基本的な failure オブジェクトテスト
	my $failure = Object::Result->create_failure();

	ok( ! $failure ,'create_failure');
	ok( blessed $failure ,'blessed create_failure');
	ok( looks_like_number( $failure->error_code ) ,'looks_like_number create_failure->error_code');
	ok( length $failure->error_message ,'length create_failure->error_message');
	ok( length "$failure" , "length \"$failure\" (stringify test)");

	# data にスカラ値が入っている場合のテスト
	my $scalar_success = Object::Result->create_success({ data => "foo" });
	ok( $scalar_success && blessed $scalar_success ,'$scalar_success && blessed $scalar_success');
	unless(
		is( $scalar_success->data
			,'foo'
			,'$scalar_success->data is "foo"')
		)
	{
		note Dumper( {data => $scalar_success->data } );
	}

	my @constructors = qw/
						create_success
						create_failure
						/;

	my @params	= (
		{
			attr		=> undef
			,keypath	=> undef
			,is			=> undef
		}
		,{
			attr		=> {data => 'foo'}
			,keypath	=> undef
			,is			=> 'foo'
		}
		,{
			attr		=> {data => {foo => 'bar'}}
			,keypath	=> [qw/foo/]
			,is			=> 'bar'
		}
		,{
			attr		=> {data => {foo => [qw/a b c/]}}
			,keypath	=> [qw/foo 1/]
			,is			=> 'b'
		}
		,{
			attr		=> {data => {foo => [{ a => 'b' ,c => 'd'},{e => 'f' ,g => 'h'}]}}
			,keypath	=> [qw/foo 1 g /]
			,is			=> 'h'
		}
	);

	my $d = sub
	{
		return Data::Dumper->new([@_])->Terse(1)->Indent(0)->Dump;
	};

	# data アクセサテスト
	for my $method ( @constructors )
	{
		for my $test ( @params )
		{
			no warnings 'uninitialized';
			my $r = Object::Result->$method( $test->{attr} );
			ok( blessed $r ,"$method (".$d->($test->{attr}).")");
			is( $r->data( @{$test->{keypath}} )
				,$test->{is}
				,"keypath : '".$d->($test->{keypath})."' is '$test->{is}'"
				) || note Dumper( {'$r->data' =>  $r->data} );
		}
	}

	done_testing();
}

