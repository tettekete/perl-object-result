package Object::Result;

=pod

=encoding utf8

=head1 NAME

Object::Result

=head1 What is Object::Result

処理結果向けの boolean 判定可能なオブジェクトを提供します。

=head1 SYNOPSIS

```perl
my $success = Object::Result->create_success();
my $failure = Object::Result->create_failure();
```

=head1 Author

- Create on : 2017/08/23
- Author : H.Seo

=cut

{
	our $VERSION = '1.0';
}

use strict;
use warnings;
use Carp qw(:DEFAULT cluck);

use overload
	 'bool'	=> 'success'
	,'""'	=> 'stringify'
;

use Scalar::Util qw(looks_like_number);

=head1 METHODS

=head3 new() - private

直接コールせず、`create_success`,`create_failure` を使用して下さい。

=cut


sub new
{
	my $class = shift @_;

	if( caller ne __PACKAGE__ )
	{
		carp "Not recomend to use new directly.";
	}

	my $self = {
					_private => {}
					,success	=> undef
				};

	bless $self,ref $class || $class;

	return $self;
}

sub _import_attr
{
	my $self = shift @_;

	my $acceptableKeys	= shift @_;
	my $inputAttr		= shift @_;

	for my $key ( @$acceptableKeys )
	{
		next if( ! exists $inputAttr->{$key} );
		$self->{_private}->{$key}	= $inputAttr->{$key};
	}

	return $self;
}


=head3 create_success( [\%attr] )

成功オブジェクトを返します。

`%attr` で参照されるキー

- `data` : オプション。任意のデータ（スカラ、リファレンス）をオブジェクトにセットします。

=cut


sub create_success
{
	my $class = shift @_;

	my @acceptable_keys = qw/
								data
							/;

	my $self = __PACKAGE__->new();

	$self->_import_attr(
							\@acceptable_keys
							,@_
						);

	$self->{success} = 1;

	return $self;
}

=head3 create_failure( [\%attr] )

失敗オブジェクトを返します。

`%attr` で参照されるキー

- `error_code` : オプション。オブジェクトのエラーコード(数値を期待)を設定できます。
- `error_message` : オプション。オブジェクトのエラーメッセージを設定できます。
- `data` : オプション。任意のデータ（スカラ、リファレンス）をオブジェクトにセットします。

=cut

sub create_failure
{
	my $class	= shift @_;

	my @acceptable_keys = qw/
								error_code
								error_message
								data
							/;

	my $self = __PACKAGE__->new();

	$self->_import_attr(
							\@acceptable_keys
							,@_
						);

	return $self;
}

sub success
{
	return $_[0]->{success} ? 1 : 0;
}

sub error_code
{
	return $_[0]->{success} ? undef : ($_[0]->{_private}->{error_code} // -1);
}

sub error_message
{
	return $_[0]->{success} ? undef : $_[0]->{_private}->{error_message} // 'Error occured';
}

sub data
{
	my $self = shift @_;

	if( @_ && defined $_[0] )
	{
		return _fetch_with_key_path( $self->{_private}->{data} , @_ );
	}
	else
	{
		return $self->{_private}->{data};
	}
}

sub stringify
{
	my $self = shift @_;

	if( $self->success )
	{
		return "OK";
	}
	else
	{
		return sprintf("Error : [%s] %s"
						,$self->error_code
						,$self->error_message
						);
	}
}


sub _fetch_with_key_path
{
	my $obj = shift @_;
	my @keyPath	= @_;

	while( @keyPath )
	{
		my $key = shift @keyPath;
		
		if( 'ARRAY' eq ref $obj
		 && $key =~ /^-{0,1}\d+$/)
		{
			$obj = $obj->[$key];
		}
		elsif( 'HASH' eq ref $obj )
		{
			$obj = $obj->{$key}
		}
		else
		{
			return undef;
		}
	}

	return $obj;
}

1;

__END__
